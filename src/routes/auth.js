const router = require('express').Router();
const { loggedIn, adminOnly } = require("../middleware/auth.middleware");
const userController = require('../controllers/user.controller');

// Register a new User
router.post('/register', userController.register);

// Login
router.post('/login', userController.login);

// Lists of users
router.get('/users', userController.getusers);
// a user
router.get('/user/:id', userController.getuser);

// update user
router.patch('/user/:id', userController.updateuser);


// Auth user only
router.get('/authuseronly', loggedIn, userController.authuseronly);

// Admin user only
router.get('/adminonly', loggedIn, adminOnly, userController.adminonly);

module.exports = router;