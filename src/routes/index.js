const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const request = require('request');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.send('<h1>GPS backend has been connected</h1>');
});

const geocode = require('../utils/geocode');
const search = require('../utils/search');
const api_key = process.env.WEATHERSTACK_API_KEY;
const base_url = process.env.WEATHERSTACK_BASE_API;

const gpsdb = require('../database/mongoURI').mongoURI;
const gpsdata = require("../database/gpsdata");

mongoose.connect(gpsdb, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log("Connected to Mongodb");
    })
    .catch((err) => {
        console.log(err);
    })


router.get('/', (req, res) => {
    res.send({ message: "Backend has been connected" });
});

router.get('/vessel-tracker', (req, res) => {
    res.send({
        message: {
            "anomaly_details":
                [
                    { "duration": 84780, "from": 1608623129, "to": 1608707909, "anomaly_array": "{109}" },
                    { "duration": 360, "from": 1608713308, "to": 1608713668, "anomaly_array": "{109}" },
                    { "duration": 180, "from": 1608719246, "to": 1608719426, "anomaly_array": "{109}" },
                    { "duration": 70380, "from": 1612155642, "to": 1612226022, "anomaly_array": "{109}" },
                    { "duration": 2879, "from": 1612227461, "to": 1612230340, "anomaly_array": "{109}" },
                    { "duration": 540, "from": 1612233762, "to": 1612234302, "anomaly_array": "{109}" },
                    { "duration": 539, "from": 1615058159, "to": 1615058698, "anomaly_array": "{109}" },
                    { "duration": 534, "from": 1615060684, "to": 1615061218, "anomaly_array": "{109}" },
                    { "duration": 369, "from": 1615101355, "to": 1615101724, "anomaly_array": "{109}" },
                    { "duration": 360, "from": 1615188838, "to": 1615189198, "anomaly_array": "{109}" },
                    { "duration": 18365, "from": 1615227899, "to": 1615246264, "anomaly_array": "{109}" },
                    { "duration": 53510, "from": 1618622623, "to": 1618676133, "anomaly_array": "{101}" },
                    { "duration": 73618, "from": 1622608481, "to": 1622682099, "anomaly_array": "{109}" },
                    { "duration": 40499, "from": 1622958579, "to": 1622999078, "anomaly_array": "{109}" },
                    { "duration": 124404, "from": 1623235581, "to": 1623359985, "anomaly_array": "{109}" },
                    { "duration": 1262, "from": 1623370602, "to": 1623371864, "anomaly_array": "{109}" },
                    { "duration": 25728, "from": 1626224280, "to": 1626250008, "anomaly_array": "{109}" },
                    { "duration": 1806, "from": 1641835283, "to": 1641837089, "anomaly_array": "{109}" }
                ],

            "last_reported":
            {
                "updated_on": 1644213755, "verification_time": 1644202600, "height_depth": "", "breadth": "44", "destinationpoint": "", "latitude": 17.6367, "vessel_tonnage": "",
                "ntds_tracktype_flag": "AH", "cargo_type": "", "in_track": "false", "anomaly_array": "{102}", "source_type_name": "MSIS", "geom": "POINT (67.0833 17.6367)", "uuid": "822e5efa-cf8f-45e9-a788-05ca541b5473",
                "speed": "10", "updated_by_name": "", "track_mil_civ_name": "CIVILIAN", "iff_flag": "", "track_type_code": "TANKER-ALL", "owner_unit_name": "DNCO", "imo_no": "9845946", "dtg": "Mon Feb 07 10:25:45 IST 2022",
                "is_tm_received_flag": "", "owner_unit_type": "DNCO", "country_name": "China", "course": 150, "call_sign": "BOLB7", "trackname": "YUAN LAN WAN",
                "updated_by_call_sign": "", "longitude": 67.0833, "updated_by_type": "", "fusion_status": "", "ntds_tracktype_desc": "Assumed Hostile",
                "mmsi_no": "412706000", "length": "250", "iff_desc": "", "navigation_status": "", "track_type_name": "TANKER", "owner_call_sign": "DNCO", "draught": "0",
                "anomaly_status": "true", "track_mil_civ_code": "CIV", "source_sub_type_name": "EXACTEARTH", "width": "", "sensor_timestamp": 1644209745, "verification_flag": "M", "status": "0"
            }
            ,

            "Voyage Details":
                [
                    { "duration": 389066, "destinationpoint": "SIKKAINDIA", "from": 1607366451, "to": 1607755517 },
                    { "duration": 749110, "destinationpoint": "SINGAPORE", "from": 1607854575, "to": 1608603685 },
                    { "duration": 1361843, "destinationpoint": "NA", "from": 1608621907, "to": 1609983750 },
                    { "duration": 1066753, "destinationpoint": "FUJAIRAH", "from": 1610025124, "to": 1611091877 },
                    { "duration": 813071, "destinationpoint": "CHIBA JAPAN", "from": 1611143674, "to": 1611956745 },
                    { "duration": 3101411, "destinationpoint": "NA", "from": 1612144853, "to": 1615246264 },
                    { "duration": 403902, "destinationpoint": "SIKKAINDIA", "from": 1615963925, "to": 1616367827 },
                    { "duration": 365386, "destinationpoint": "MALE-MALDIVES", "from": 1616372869, "to": 1616738255 },
                    { "duration": 538605, "destinationpoint": "GALLE", "from": 1616738395, "to": 1617277000 },
                    { "duration": 1759, "destinationpoint": "GALLE", "from": 1617277162, "to": 1617278921 },
                    { "duration": 121, "destinationpoint": "GALLE", "from": 1617279041, "to": 1617279162 },
                    { "duration": 1066294, "destinationpoint": "RAS LAFFAN", "from": 1617279401, "to": 1618345695 },
                    { "duration": 10625, "destinationpoint": "RAS LAFFAN", "from": 1618433356, "to": 1618443981 },
                    { "duration": 781445, "destinationpoint": "JAPAN CHIBA", "from": 1618444099, "to": 1619225544 },
                    { "duration": 1049406, "destinationpoint": "NA", "from": 1622608481, "to": 1623657887 },
                    { "duration": 189724, "destinationpoint": "YANBU", "from": 1623746805, "to": 1623936529 },
                    { "duration": 9642, "destinationpoint": "NA", "from": 1624352869, "to": 1624362511 },
                    { "duration": 6296, "destinationpoint": "YANBU", "from": 1624473112, "to": 1624479408 },
                    { "duration": 281336, "destinationpoint": "ARMED GUARD ON BOARD", "from": 1624479771, "to": 1624761107 },
                    { "duration": 109802, "destinationpoint": "SUEZ CANAL", "from": 1625071074, "to": 1625180876 },
                    { "duration": 83854, "destinationpoint": "MARSEILLE", "from": 1625211501, "to": 1625295355 },
                    { "duration": 1607586, "destinationpoint": "PORT SAID", "from": 1626068998, "to": 1627676584 },
                    { "duration": 899429, "destinationpoint": "LE HAVRE", "from": 1635083569, "to": 1635982998 },
                    { "duration": 1804709, "destinationpoint": "SINES", "from": 1636013496, "to": 1637818205 },
                    { "duration": 630588, "destinationpoint": "MILFORD HAVEN", "from": 1637826059, "to": 1638456647 },
                    { "duration": 429345, "destinationpoint": "GIBRALTAR", "from": 1638458782, "to": 1638888127 },
                    { "duration": 221159, "destinationpoint": "GIBRALTAR", "from": 1638989106, "to": 1639210265 },
                    { "duration": 371986, "destinationpoint": "CANAKKALE", "from": 1639212125, "to": 1639584111 },
                    { "duration": 432199, "destinationpoint": "MALTA", "from": 1639585012, "to": 1640017211 },
                    { "duration": 422711, "destinationpoint": "MELLITAH", "from": 1640024407, "to": 1640447118 },
                    { "duration": 435420, "destinationpoint": "MALTA OPL", "from": 1640658278, "to": 1641093698 },
                    { "duration": 397092, "destinationpoint": "PORT SAID", "from": 1641094937, "to": 1641492029 },
                    { "duration": 1806, "destinationpoint": "NA", "from": 1641835283, "to": 1641837089 },
                    { "duration": 6852, "destinationpoint": "JEBEL ALI UAE", "from": 1641837806, "to": 1641844658 },
                    { "duration": 760, "destinationpoint": "ARM GUARDS ON BOARD", "from": 1641847530, "to": 1641848290 },
                    { "duration": 1079, "destinationpoint": "JEBEL ALI UAE", "from": 1641849326, "to": 1641850405 }
                ],

            "kinematic_details":
                [
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-02", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-03", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-04", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-05", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-06", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-07", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-08", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-09", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-10", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-11", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-12", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-13", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-15", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-16", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-17", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-18", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-19", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-20", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-21", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-22", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-23", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-24", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-25", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-26", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-27", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-28", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-29", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-30", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-31", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-32", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-33", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-34", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-35", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-36", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-37", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-38", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 },
                    { "date": "2022-02-39", "distanceTravelled": 0.7378405691778637, "avgSpeed": 0 },
                    { "date": "2022-02-40", "distanceTravelled": 115.84660674963507, "avgSpeed": 8.6916668475 },
                    { "date": "2022-02-41", "distanceTravelled": 83.23966714798775, "avgSpeed": 10.918181763636364 },
                    { "date": "2022-02-42", "distanceTravelled": 33.772553785618285, "avgSpeed": 10.5666666 }
                ],

            "verification_details":
                [
                    { "verification_time": 1618544002, "verification_flag": "I" },
                    { "verification_time": 1618551253, "verification_flag": "I" },
                    { "verification_time": 1618552053, "verification_flag": "I" },
                    { "verification_time": 1618568587, "verification_flag": "I" },
                    { "verification_time": 1618627312, "verification_flag": "I" },
                    { "verification_time": 1618633642, "verification_flag": "I" },
                    { "verification_time": 1618634312, "verification_flag": "I" },
                    { "verification_time": 1618634964, "verification_flag": "I" },
                    { "verification_time": 1618635533, "verification_flag": "I" },
                    { "verification_time": 1618635773, "verification_flag": "I" },
                    { "verification_time": 1618671484, "verification_flag": "I" },
                    { "verification_time": 1618672192, "verification_flag": "I" },
                    { "verification_time": 1618672664, "verification_flag": "I" },
                    { "verification_time": 1618673372, "verification_flag": "I" },
                    { "verification_time": 1618673894, "verification_flag": "I" },
                    { "verification_time": 1618674552, "verification_flag": "I" },
                    { "verification_time": 1618675133, "verification_flag": "I" },
                    { "verification_time": 1618675193, "verification_flag": "I" },
                    { "verification_time": 1618676333, "verification_flag": "I" },
                    { "verification_time": 1618730901, "verification_flag": "I" },
                    { "verification_time": 1618731238, "verification_flag": "I" },
                    { "verification_time": 1618734948, "verification_flag": "I" },
                    { "verification_time": 1618735474, "verification_flag": "I" },
                    { "verification_time": 1618751218, "verification_flag": "I" },
                    { "verification_time": 1618752669, "verification_flag": "I" },
                    { "verification_time": 1618753825, "verification_flag": "I" },
                    { "verification_time": 1618754856, "verification_flag": "I" },
                    { "verification_time": 1618755470, "verification_flag": "I" },
                    { "verification_time": 1618756400, "verification_flag": "I" },
                    { "verification_time": 1618759192, "verification_flag": "I" },
                    { "verification_time": 1618760123, "verification_flag": "I" },
                    { "verification_time": 1618762602, "verification_flag": "I" },
                    { "verification_time": 1618763223, "verification_flag": "I" },
                    { "verification_time": 1618764460, "verification_flag": "I" },
                    { "verification_time": 1618765701, "verification_flag": "I" },
                    { "verification_time": 1618766321, "verification_flag": "I" },
                    { "verification_time": 1618766630, "verification_flag": "I" },
                    { "verification_time": 1618767872, "verification_flag": "I" },
                    { "verification_time": 1618768800, "verification_flag": "I" },
                    { "verification_time": 1618769421, "verification_flag": "I" },
                    { "verification_time": 1618770357, "verification_flag": "I" }
                ]
        }
    });
});
router.get('/getgpsdata', (req, res) => {
    res.send({
        message: { body: req.body, query: req.query },
        data: "gps"
    })
})
router.post('/gpsdetails', (req, res) => {
    const { assetnumber, latitude, longitude } = req.body;
    res.send({ message: { assetnumber, latitude, longitude } });
    // let url = "http://13.126.20.121:5000";

    // request({ url: url, json: true }, (error, response) => {
    //     if (error) {
    //         throw error;
    //     }
    // });

});


router.post("/addgpsdata", (req, res) => {
    const { assetnumber, latitude, longitude } = req.body;
    let errors = [];
    console.log(assetnumber, latitude, longitude)
    if (!assetnumber || !latitude || !longitude) {
        errors.push({ msg: "Parameters are missing" });
    }
    if (errors.length > 0) {
        res.json({ Message: errors })
    } else {
        res.json({ message: "wehave reciced" })
        // const newgpsdata = new gpsdata({
        //     assetnumber,
        //     latitude,
        //     longitude
        // });

        // newgpsdata.save().then(newgpsdata => {
        //     console.log(newgpsdata)
        //     res.json({ Message: "Data Inserted" });
        // }).catch(err => console.log(err));
    }
});




router.get('/search/:place', (req, res) => {
    const place = req.params.place;
    geocode(place, (error, { longitude, latitude, details } = {}) => {
        const url = `${base_url}/current?access_key=${api_key}&query=${latitude},${longitude}`;
        request({ url: url, json: true }, (error, response) => {
            res.send({
                longitude,
                latitude,
                details,
                weather_report: response.body
            })
        });
    });
})

module.exports = router;
