const bcrypt = require('bcryptjs/dist/bcrypt');
const mongoose = require('mongoose');
const validator = require('validator').default;
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isStrongPassword(value)) {
                throw new Error("Invalid Email")
            }
            return validator.isEmail(value)
        }
    },
    password: {
        type: String,
        required: true,
        validate(value) {
            if (!validator.isStrongPassword(value)) {
                throw new Error("not a strong password")
            }
        }
    },
    mobile: {
        type: String,
    },
    registerd_date: {
        type: Date,
        default: Date.now(),
    },
    user_type: {
        type: Number,
    }
});

// find by credentials
userSchema.statics.findByCredentials = async (email_or_mobile, password) => {
    const user = await User.findOne({ email_or_mobile });
    if (!user) throw new Error("Not a valid email id !");

    const validPass = await bcrypt.compare(password, user.password);
    if (!validPass) return res.status(400).send("Wrong Password is wrong");

}

// userSchema.pre('save', async function (next) {

// })

const User = mongoose.model("users", userSchema);
module.exports = User;