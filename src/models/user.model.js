const mongoose = require('mongoose');
const validator = require('validator').default;
const { NotFoundError } = require("../middleware/utility");
const bcrypt = require("bcryptjs");
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Invalid Email")
            }
        }
    },
    password: {
        type: String,
        required: true,
        validate(value) {
            if (!validator.isStrongPassword(value)) {
                throw new Error("not a strong password")
            }
        }
    },
    mobile: {
        type: String,
    },
    registerd_date: {
        type: Date,
        default: Date.now(),
    },
    status: {
        type: Number,
    }
});
// find by credentials
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email });
    console.log(user)
    if (!user) throw new Error("Not a registered email id !");

    const validPass = await bcrypt.compare(password, user.password);
    if (!validPass) throw new Error("Wrong password !");

    return user
}

// userSchema.pre('save', async function (next) {

// })
const User = mongoose.model("users", userSchema);

// constructor
// const User = function (user) {
//     if (typeof user.id != 'undefined') {
//         this.id = user.id;
//     }
//     this.mobile = user.mobile;
//     this.email = user.email;
//     this.name = user.name;
//     this.password = user.password;
//     this.status = user.status;
//     if (typeof user.created_at != 'undefined') {
//         this.created_at = user.created_at;
//     }
//     if (typeof user.updated_at != 'undefined') {
//         this.updated_at = user.updated_at;
//     }
// };

// User.create = async (newUser) => {
//     let name = newUser.name;
//     delete newUser.name;
//     // here the monogodb saving code goes

// };

// User.findBy = async (data, field, attach_additional_data = false) => {
//     // here the monogodb saving code goes
//     if (row.length) {
//         let user = {}

//         return user;
//     }
//     else {
//         throw new NotFoundError("User does not exist");
//     }
// };

// User.login = async (value) => {
//     // monogdb code for successful login
// };

// User.getAll = async (start, limit, return_total, sort_data) => {
//     // monogodb users data
// };

// User.getType = async (id) => {
//     // monogo db code get type
// };

module.exports = User;