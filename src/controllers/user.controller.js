const User = require("../models/user.model.js");
const config = require("../utils/config");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { NotFoundError } = require("../middleware/utility");
const { password } = require("pg/lib/defaults");


// Register a new User
exports.register = async (req, res) => {

    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hasPassword = await bcrypt.hash(req.body.password, salt);

    // Save User in the database - monogodb
    try {
        // Create an user object
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hasPassword,
            mobile: req.body.mobile,
            registerd_date: new Date(Date.now()).toUTCString(),
            status: req.body.status || 1
        });
        await user.save();
        res.status(200).send({
            user_details: user,
            message: `User added successfully with ID: ${user._id}`
        });
    }
    catch (err) {
        res.status(500).send("Unable to register")
    }
};

// Get  Users
exports.getusers = async (req, res) => {
    try {
        // get all the users
        const users = await User.find({});
        res.status(200).send(users)
    }
    catch (err) {
        res.status(500).send(e)
    }
};

// Get  User
exports.getuser = async (req, res) => {
    const _id = req.params.id;
    try {
        // get an the user
        const user = await User.findById(_id);
        if (!user) res.status(404).send("No user found")
        res.status(200).send(user)
    }
    catch (err) {
        res.status(500).send(err)
    }
};

// update a user
exports.updateuser = async (req, res) => {
    const _id = req.params.id;
    const update = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'mobile', 'status'];
    const isValidRequest = update.every((ele) => allowedUpdates.includes(ele))
    if (!isValidRequest) res.status(400).send({ error: "Invalid updates !" });

    try {
        const user = await User.findByIdAndUpdate(_id, req.body, { new: true, runValidators: true })
        if (!user) res.status(404).send("No user found")
        res.status(200).send({ user: user, message: "User has been updated" })
    } catch (error) {
        res.status(500).send(error)
    }

}


// Login
exports.login = async (req, res) => {

    try {
        // Check user exist

        const user = await User.findByCredentials(req.body.email_or_mobile, req.body.password);
        console.log(user)
        res.send(user)
        // const user = await User.login(req.body.mobile_or_email);
        // if (user) {
        //     const validPass = await bcrypt.compare(req.body.password, user.password);
        //     if (!validPass) return res.status(400).send("Mobile/Email or Password is wrong");

        //     // Create and assign token
        //     const token = jwt.sign({ id: user.id, user_type_id: user.user_type_id }, config.TOKEN_SECRET);
        //     res.header("auth-token", token).send({ "token": token });
        //     // res.send("Logged IN");
        // }
    }
    catch (error) {
        res.status(500).send({ error: "Check email/mobile or password" });
        // if (err instanceof NotFoundError) {
        //     res.status(401).send(`Mobile/Email or Password is wrong`);
        // }
        // else {
        //     let error_data = {
        //         entity: 'User',
        //         model_obj: { param: req.params, body: req.body },
        //         error_obj: err,
        //         error_msg: err.message
        //     };
        //     res.status(500).send("Error retrieving User");
        // }
    }

};

// Access auth users only
exports.authuseronly = (req, res) => {
    res.send("Hey,You are authenticated user. So you are authorized to access here.");
};

// Admin users only
exports.adminonly = (req, res) => {
    res.send("Success. Hellow Admin, this route is only for you");
};