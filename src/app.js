var express = require('express');
var path = require('path');
var logger = require('morgan');
require('dotenv').config();

var indexRouter = require('./routes/index');
var authRouter = require('./routes/auth');

var app = express();

// app.use(bodyParser.json());       // to support JSON-encoded bodies
// app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
//     extended: true
// }));

app.use(express.urlencoded()); // to support URL-encoded bodies

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', indexRouter);
app.use('/auth', authRouter);

module.exports = app;