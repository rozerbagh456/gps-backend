const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');

const app = express();

// Postgresql
// const { Client } = require('pg')
// const client = new Client()
// client.connect()
// client.query('SELECT $1::text as message', ['Hello world!'], (err, res) => {
//     console.log(err ? err.stack : res.rows[0].message) // Hello World!
//     client.end()
// })

// app.set('view engine', 'ejs')

// localhost connection mongodb.
const uri = "mongodb://localhost:27017/practice-db";
const daName = ''
MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }).then(client => {
    const db = client.db('movie-db');
    const movies = db.collection('movies');
    // console.log(movies)



    app.use(bodyParser.urlencoded({ extended: true }))
    app.get('/', (req, res) => {

        movies.find().toArray()
            .then(results => {
                console.log(results)
                res.render('index.ejs', { movies: results })
            })
            .catch(error => console.error(error))
        res.status(200).send('Hello World, Welcome to my blog!');
    });
    /* 
    C - create
    R - replace
    U - update
    D - delete
    */
    app.post('/movies', (req, res) => {

        movies.insertOne(req.body).then(result => {
            res.redirect('/')
        }).catch(error => console.error(error))
    });

    app.post('/show-movies-id', (req, res) => {
        console.log(req.body)
        res.redirect('/')
    })
    app.listen(8002, function () {
        console.log('listening on 8002')
    })
})

